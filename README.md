Koha Cookbook
=============

This is *not* examples for Koha developers, this is about food!

If you are looking for developers information have a look at this [wiki page](https://wiki.koha-community.org/wiki/Getting_involved)

The cookbook is generated with Sphinx.

Setup
-----

> sudo apt-get install make python3-sphinx python3-sphinxcontrib.spelling python3-pip

Outputing Docs
--------------

### For HTML
> make html

### For HTML in a single page
>  make singlehtml

### For an epub
>  make epub

Other useful commands
---------------------

To find everything that you can do, you can just do

> make
